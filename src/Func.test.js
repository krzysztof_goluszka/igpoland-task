import {
  setSortType,
  setMultipleSort,
  sortData,
  calculateValue
} from "./Accounts";

it("Function setSortType", () => {
  let newAscendingType = setSortType({ type: "ascending" }, "ascending");

  let updatedAscendingType = setSortType({ type: "descending" }, "ascending");

  let updatedDescendingType = setSortType({ type: "ascending" }, "descending");

  describe(" should return undefined if value is same as ", () =>
    expect(newAscendingType).toBeUndefined());

  describe(" and set different type", () =>
    expect(updatedAscendingType).toEqual("ascending"));

  describe(" or", () => expect(updatedDescendingType).toEqual("descending"));
});

it("Function setMultipleSort", () => {
  let sameSortChecked = setMultipleSort(
    [{ by: "name", type: "ascending" }],
    "name",
    "ascending"
  );

  expect(sameSortChecked).toEqual([]);
});

let fakeData = [
  {
    _id: "5d9ddef4915161280000853b",
    id: 1,
    name: "Spread bet",
    default: true,
    funds: 10000,
    profitLoss: 0.23,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  },
  {
    _id: "5d9ddef4915161280000853c",
    id: 2,
    name: "New Spread bet",
    default: false,
    funds: 1000,
    profitLoss: -679,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  }
];

let fakeDataSortedDescending = [
  {
    _id: "5d9ddef4915161280000853c",
    id: 2,
    name: "New Spread bet",
    default: false,
    funds: 1000,
    profitLoss: -679,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  },
  {
    _id: "5d9ddef4915161280000853b",
    id: 1,
    name: "Spread bet",
    default: true,
    funds: 10000,
    profitLoss: 0.23,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  }
];

let fakeDataSortedAscending = [
  {
    _id: "5d9ddef4915161280000853b",
    id: 1,
    name: "Spread bet",
    default: true,
    funds: 10000,
    profitLoss: 0.23,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  },
  {
    _id: "5d9ddef4915161280000853c",
    id: 2,
    name: "New Spread bet",
    default: false,
    funds: 1000,
    profitLoss: -679,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  }
];

it("Sorts data by name type descending ", () => {
  sortData(["name"], ["descending"], fakeData);
  expect(fakeData).toEqual(fakeDataSortedDescending);
});

it("Sorts data by name type ascending ", () => {
  sortData(["name"], ["ascending"], fakeData);
  expect(fakeData).toEqual(fakeDataSortedAscending);
});

let fakeDataTwoSorts = [
  {
    _id: "5d9ddef4915161280000853b",
    id: 1,
    name: "Spread bet",
    default: true,
    funds: 10000,
    profitLoss: 0.23,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  },
  {
    _id: "5d9ddef4915161280000853c",
    id: 2,
    name: "Spread bet",
    default: false,
    funds: 1000,
    profitLoss: -679,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  }
];

let fakeDataTwoSortsSorted = [
  {
    _id: "5d9ddef4915161280000853c",
    id: 2,
    name: "Spread bet",
    default: false,
    funds: 1000,
    profitLoss: -679,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  },
  {
    _id: "5d9ddef4915161280000853b",
    id: 1,
    name: "Spread bet",
    default: true,
    funds: 10000,
    profitLoss: 0.23,
    accountType: "IGSB",
    isDemo: false,
    currency: "$"
  }
];

it("Sorts data by name type descending and by profit & loss descending ", () => {
  sortData(
    ["name", "profitLoss"],
    ["descending", "descending"],
    fakeDataTwoSorts
  );
  expect(fakeDataTwoSorts).toEqual(fakeDataTwoSortsSorted);
});

it("Calculate value from current currency", () => {
  let convertedValue = calculateValue("€", 10);
  const resultValue = "11.00";
  expect(convertedValue).toEqual(resultValue);
});
