const IGPOLAND_URI = "https://recruitmentdb-508d.restdb.io/rest";

function get(url) {
  return window
    .fetch(IGPOLAND_URI + url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-apikey": "5d9f48133cbe87164d4bb12c"
      }
    })
    .then(res => {
      if (res.status === 200) {
        return res.json().then(data => data);
      }
      return res.text().then(err => console.log(err));
    })
    .catch(err => {
      return console.log(err);
    });
}
//asd

export function getAccounts(page) {
  let skip = page * 1000;
  return get("/accounts-v-2?max=" + 1000 + "&skip=" + skip + "&totals=true");
}

export function getAccountTypes() {
  return get("/accounttypes");
}
