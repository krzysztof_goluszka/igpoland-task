import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

it("renders <App /> correctly", () => {
  const { getByText } = render(<App />);
  expect(getByText("AccountList")).toBeInTheDocument();
});
