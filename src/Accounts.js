import React from "react";
import { Grid } from "react-styled-flexboxgrid";

import Layout from "./component/Layout";
import Table from "./component/Table";

import { getAccounts, getAccountTypes } from "./Api";

let initialState = {
  error: false,
  accounts: {
    loading: false,
    data: [],
    totals: 0
  },
  accountTypes: {
    loading: false,
    data: {}
  },
  sort: [],
  filters: {
    names: [],
    accountTypes: []
  },
  search: "New",
  currency: "$",
  currentPage: 0
};

export let setSortType = (activeSort, type) => {
  return activeSort.type === type ? undefined : type;
};

let setSort = (activeSort, by, type) => {
  return activeSort.by === by
    ? {
        ...activeSort,
        type: setSortType(activeSort, type)
      }
    : activeSort;
};

export let setMultipleSort = (activeSort, by, type) => {
  let isInActiveSort = activeSort.map(singleSort => singleSort.by).includes(by);

  return isInActiveSort
    ? activeSort.map(s => setSort(s, by, type)).filter(s => s.type)
    : activeSort.concat([{ by, type }]);
};

let getSort = activeSort => {
  return activeSort.reduce(
    (acc, s) => {
      return [acc[0].concat(s.by), acc[1].concat(s.type)];
    },
    [[], []]
  );
};

export let sortData = (sortBy, sortType, data) => {
  let [firstSortBy, secondSortBy, thirdSortBy] = sortBy;
  let [firstSortType, secondSortType, thirdSortType] = sortType;

  if (firstSortBy === "undefined") return data;

  let getValueOfSort = (sortType, firstValueGreater) => {
    if (sortType === "descending" && firstValueGreater === true) return 1;
    else if (sortType === "descending" && firstValueGreater === false)
      return -1;
    else if (sortType === "ascending" && firstValueGreater === true) return -1;
    else if (sortType === "ascending" && firstValueGreater === false) return 1;
  };

  data.sort((a, b) => {
    if (firstSortBy !== "undefined") {
      if (a[firstSortBy] > b[firstSortBy])
        return getValueOfSort(firstSortType, true);
      if (a[firstSortBy] < b[firstSortBy])
        return getValueOfSort(firstSortType, false);
    }

    if (secondSortBy !== "undefined") {
      if (a[secondSortBy] > b[secondSortBy])
        return getValueOfSort(secondSortType, true);
      if (a[secondSortBy] < b[secondSortBy])
        return getValueOfSort(secondSortType, false);
    }

    if (thirdSortBy !== "undefined") {
      if (a[thirdSortBy] > b[thirdSortBy])
        return getValueOfSort(thirdSortType, true);
      if (a[thirdSortBy] < b[thirdSortBy])
        return getValueOfSort(thirdSortType, false);
    }

    return 0;
  });
};

export let calculateValue = (activeCurrency, value) => {
  let newValue = activeCurrency === "$" ? value * 0.9 : value * 1.1; //Currency values for 16.01.2020
  return newValue.toFixed(2);
};

let getValues = (activeCurrency, data) => {
  return data.map(e => {
    e.profitLoss =
      e.currency === activeCurrency
        ? e.profitLoss
        : calculateValue(activeCurrency, e.profitLoss);
    return e;
  });
};

let getData = (dispatch, currentPage) => {
  dispatch({ type: "loadData" });

  return getAccounts(currentPage)
    .then(data => {
      console.log("1", data);
      dispatch({
        type: "saveAccounts",
        payload: { data: data.data, totals: data.totals.total }
      });
      return getAccountTypes()
        .then(data => {
          let dataToPayload = new Map();
          data.forEach(e => {
            dataToPayload.set(e.id, e.title);
          });
          dispatch({ type: "saveTypes", payload: dataToPayload });
          dispatch({ type: "mergeData" });
          dispatch({ type: "calculateValues" });
        })
        .catch(() => dispatch({ type: "throwError" }));
    })
    .catch(() => dispatch({ type: "throwError" }));
};

let reducer = (state, action) => {
  switch (action.type) {
    case "throwError":
      return {
        ...state,
        error: true
      };

    case "loadData":
      return {
        ...state,
        accounts: {
          ...state.accounts,
          loading: true
        },
        accountTypes: {
          ...state.accountTypes,
          loading: true
        }
      };

    case "saveAccounts":
      return {
        ...state,
        accounts: {
          ...state.accounts,
          loading: true,
          data: action.payload.data,
          totals: action.payload.totals
        }
      };

    case "saveTypes":
      return {
        ...state,
        accountTypes: {
          ...state.accountTypes,
          loading: false,
          data: action.payload
        }
      };

    case "mergeData":
      return {
        ...state,
        accounts: {
          ...state.accounts,
          data: state.accounts.data.map(e => {
            e.accountType = state.accountTypes.data.get(e.accountType);
            return e;
          })
        }
      };

    case "setSort":
      return {
        ...state,
        sort: setMultipleSort(
          state.sort,
          action.payload.by,
          action.payload.type
        )
      };

    case "calculateValues":
      return {
        ...state,
        accounts: {
          ...state.accounts,
          loading: false,
          data: getValues(state.currency, state.accounts.data)
        }
      };

    case "setNextPage":
      return {
        ...state,
        currentPage:
          (state.currentPage + 1) * 1000 > state.accounts.totals
            ? state.currentPage
            : state.currentPage + 1 //Todo: check
      };

    case "setPreviousPage":
      return {
        ...state,
        currentPage:
          state.currentPage >= 1 ? state.currentPage - 1 : state.currentPage
      };

    default:
      return state;
  }
};

let Accounts = () => {
  let [state, dispatch] = React.useReducer(reducer, initialState);

  let [sortBy, sortType] = getSort(state.sort);

  React.useEffect(() => {
    getData(dispatch, state.currentPage);
  }, [state.currentPage]);

  let accounts = state.accounts;

  sortData(sortBy, sortType, accounts.data);

  let maxPage = Math.ceil(state.accounts.totals / 1000);

  return (
    <Grid>
      <Layout>
        <Table
          currency={state.currency}
          data={accounts.data}
          loading={accounts.loading}
          sortBy={state.sortBy}
          onSetSort={(by, type) =>
            dispatch({ type: "setSort", payload: { by, type } })
          }
          activeSort={state.sort}
          onNextPage={() => dispatch({ type: "setNextPage" })}
          onPreviousPage={() => dispatch({ type: "setPreviousPage" })}
          currentPage={state.currentPage}
          maxPage={maxPage}
        />
      </Layout>
    </Grid>
  );
};

export default Accounts;
