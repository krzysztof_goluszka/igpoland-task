import React from "react";
import styled from "styled-components";

import Accounts from "./Accounts";

const Wrapper = styled.div`
  width: 100vw;
  min-height: 100vh;
  height: 1px;
  display: flex;
  justify-content: center;
  background-color: #f5f6f7;
`;

function App() {
  return (
    <Wrapper>
      <Accounts />
    </Wrapper>
  );
}

export default App;
