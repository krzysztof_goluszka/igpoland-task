import React from "react";
import styled from "styled-components";
import { Row, Col } from "react-styled-flexboxgrid";

import Arrows from "./Arrows";
import Spinner from "./Spinner";

const LabelRow = props => {
  const RowWrapper = styled.div`
    width: 100%;
    display: flex;
    border-bottom: 2px solid #333;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    font-size: 1.2rem;
  `;

  const RowTitle = styled.div`
    display: flex;
    justify-content: start;
    align-items: center;
  `;

  return (
    <Row>
      <RowWrapper>
        {props.headers.map(header => {
          return (
            <Col xs={4} key={header.id}>
              <RowTitle>
                <Arrows
                  headerId={header.id}
                  onAscending={() => props.onSetSort(header.id, "ascending")}
                  onDescending={() => props.onSetSort(header.id, "descending")}
                  activeSort={props.activeSort}
                />
                {header.name}
              </RowTitle>
            </Col>
          );
        })}
      </RowWrapper>
    </Row>
  );
};

const DataRow = props => {
  const RowWrapper = styled.div`
    width: 100%;
    display: flex;
    border-bottom: 1px solid #333;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    font-size: 1.1rem;
  `;

  return (
    <Row>
      <RowWrapper key={props.id}>
        <Col xs={4}>{props.name}</Col>
        <Col xs={4}>
          {props.currency} {props.profitLoss}
        </Col>
        <Col xs={4}>{props.accountType}</Col>
      </RowWrapper>
    </Row>
  );
};

const Table = props => {
  const ContentWrapper = styled.div`
    width: 100%;
    height: 100%;
  `;

  const TableTitle = styled.h2`
    width: 100%;
    text-align: center;
  `;

  let headers = [
    { id: "name", name: "Name" },
    { id: "profitLoss", name: "Profit & Loss" },
    { id: "accountType", name: "Account Type" }
  ];

  return (
    <ContentWrapper>
      <Row>
        <Col xs={12}>
          <TableTitle>Accounts</TableTitle>
        </Col>
        <Col xs={12}>
          <TableTitle>
            Page: {props.currentPage} {" / "} {props.maxPage}
          </TableTitle>
        </Col>
      </Row>
      <Row>
        {props.loading ? (
          <Spinner />
        ) : (
          <Col xs={10} xsOffset={1}>
            <LabelRow
              headers={headers}
              sortBy={props.sortBy}
              onSetSort={props.onSetSort}
              activeSort={props.activeSort}
            />
            {props.data.map(e => {
              return (
                <DataRow
                  key={e.id}
                  currency={props.currency}
                  name={e.name}
                  profitLoss={e.profitLoss}
                  accountType={e.accountType}
                />
              );
            })}
          </Col>
        )}
      </Row>
      <Row>
        <Col xs={1} xsOffset={5}>
          <button onClick={props.onPreviousPage}> {"<"} </button>
        </Col>
        <Col xs={1}>
          <button onClick={props.onNextPage}>{">"} </button>
        </Col>
      </Row>
    </ContentWrapper>
  );
};

export default Table;
