import React from "react";
import styled from "styled-components";
import { Row, Col } from "react-styled-flexboxgrid";

import logo from "../assets/ig-logo.jpg";

const HeaderBar = styled.div`
  height: 80px;
  margin-top: 1rem;
  background-color: #fff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 5px;
  overflow: hidden;
  box-shadow: 5px 5px 10px -10px #c9cde0;
`;

const TaskTitle = styled.h3`
  margin-right: 1rem;
  letter-spacing: 0.07rem;
`;

const Logo = styled.img.attrs({
  src: logo
})`
  height: 100%;
  width: auto;
`;

const SideBar = styled.div`
  height: 100%;
  margin-top: 2rem;
  padding-top: 1rem;
  padding-left: 1rem;
  padding-right: 1rem;
  border-radius: 5px;
  overflow: hidden;
  background-image: radial-gradient(#dc001d, #900011);
  box-shadow: 5px 5px 10px -10px #c9cde0;
`;

const Content = styled.div`
  height: 100%;
  margin-top: 2rem;
  padding-top: 1rem;
  background-color: #fff;
  border-radius: 5px;
  overflow: hidden;
  box-shadow: 5px 5px 10px -10px #c9cde0;
`;

const SideBarWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const AccountsTitle = styled.p`
  font-size: 1.5rem;
  width: 100%;
  text-align: center;
  color: #fff;
`;

const Layout = props => {
  return (
    <>
      <Row>
        <Col xs={12}>
          <HeaderBar>
            <Logo />
            <TaskTitle>
              Krzysztof Gołuszka - Interview task for IG Poland
            </TaskTitle>
          </HeaderBar>
        </Col>
      </Row>
      <Row>
        <Col xs={3}>
          <SideBar>
            <SideBarWrapper>
              <AccountsTitle>AccountList</AccountsTitle>
            </SideBarWrapper>
          </SideBar>
        </Col>
        <Col xs={9}>
          <Content>{props.children}</Content>
        </Col>
      </Row>
    </>
  );
};

export default Layout;
