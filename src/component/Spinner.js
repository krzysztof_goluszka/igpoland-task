import React from "react";
import styled from "styled-components";

import spinner from "../assets/spinner.svg";

const SpinnerAnimation = styled.img.attrs({
  src: spinner
})`
  @keyframes spinner {
    to {
      transform: rotate(-360deg);
    }
  }
  position: relative;
  width: 64px;
  height: 64px;
  animation: spinner 2s linear infinite;
`;

const SpinnerWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

let Spinner = () => {
  return (
    <SpinnerWrapper>
      <SpinnerAnimation />
    </SpinnerWrapper>
  );
};

export default Spinner;
