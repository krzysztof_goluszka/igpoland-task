import React from "react";
import styled from "styled-components";

const ArrowIcon = styled.i`
  display: inline-block;
  border-color: ${props => (props.selected ? "black" : "gray")};
  border-style: solid;
  border-width: 0 2px 2px 0;
  padding: 3px;
  cursor: pointer;
  transform: ${props =>
    props.direction === "up" ? "rotate(225deg)" : "rotate(45deg)"};

  &:hover {
    border-color: #900011;
  }
`;

const ArrowsContainer = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin-right: 0.5rem;
`;

const Arrows = props => {
  let checkIsActive = (activeSort, headerId, arrowSortType) => {
    return activeSort.find(
      singleSort =>
        singleSort.by === headerId && singleSort.type === arrowSortType
    );
  };

  return (
    <ArrowsContainer>
      <ArrowIcon
        onClick={props.onAscending}
        selected={checkIsActive(props.activeSort, props.headerId, "ascending")}
        direction={"up"}
      />
      <ArrowIcon
        onClick={props.onDescending}
        selected={checkIsActive(props.activeSort, props.headerId, "descending")}
        direction={"down"}
      />
    </ArrowsContainer>
  );
};

export default Arrows;
